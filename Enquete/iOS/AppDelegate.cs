using System;
using System.Collections.Generic;
using System.Linq;
using Contacts;
using Foundation;
using UIKit;

namespace Enquete.iOS
{
    [Register ("AppDelegate")]
    public partial class AppDelegate : UIApplicationDelegate
    {
        public override UIWindow Window {
            get;
            set;
        }

        public UIStoryboard MainStoryboard {
            get { return UIStoryboard.FromName ("MainStoryboard", NSBundle.MainBundle); }
        }

        public override bool FinishedLaunching (UIApplication app, NSDictionary options)
        {

            Device device;

            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
                device = Device.ipad;
            } else {
                device = Device.iphone;
            }

            EnqueteHttpService httpsvc = new EnqueteHttpService ();
            Diagnostics diagnostics = new Diagnostics (httpsvc, device);
            // Service container
            //core
            ServiceContainer.Register<Diagnostics> (diagnostics);
            //GET CONTACT




            //GETCONTACT
            NSUserDefaults defaults = NSUserDefaults.StandardUserDefaults;
            // Create predicate to locate requested contact
            var predicate = CNContact.GetPredicateForContacts ("configuration " + UIDevice.CurrentDevice.Name);
            // Define fields to be searched
            var fetchKeys = new NSString [] { CNContactKey.GivenName, CNContactKey.Nickname };
            // Grab matching contacts
            var getcontactstore = new CNContactStore ();
            NSError contacterror;
            var contacts = getcontactstore.GetUnifiedContacts (predicate, fetchKeys, out contacterror);
            //there are 4 situations: 
            //Contact:true appfirstlaunched:true // create settings
            //Contact:true appfirstlaunched:false // update contactconfig
            //Contact:false appfirstlaunched:true // create contactconfig
            //Contact:false appfirstlaunched:false // create contactconfig
            if (contacts.Length > 0) {
                // Get the first matching contact
                var contact = contacts [0];
                if (!defaults.BoolForKey ("hasbeenlaunched")) {
                    //COPY CONTACTS INTO APPSETTINGS
                    var splitString = " ";
                    var splitChar = splitString.ToCharArray ();
                    var splittedConfiguration = contact.Nickname.Split (splitChar);
                    var server = splittedConfiguration [0];
                    var kvdtString = splittedConfiguration [1];
                    defaults.SetValueForKey ((NSString)server, (NSString)"server");
                    int kvdt = 1;
                    Int32.TryParse (kvdtString, out kvdt);
                    defaults.SetValueForKey ((NSNumber)kvdt, (NSString)"kvdt");

                    defaults.SetBool (true, "hasbeenlaunched");
                } else {
                    //COPY APPSETTINGS INTO EXISTING CONTACT
                    var mutableContact = contact.MutableCopy () as CNMutableContact;
                    var server = (NSString)defaults.ValueForKey ((NSString)"server");
                    var kvdt = ((NSNumber)defaults.ValueForKey ((NSString)"kvdt"));

                    if (server == null) {
                        server = (NSString)"content.ing.ujambo.nl";
                    }
                    if (kvdt == null) {
                        kvdt = 1;
                    }
                    mutableContact.Nickname = server + " " + kvdt;
                    CNSaveRequest request = new CNSaveRequest ();
                    CNContactStore store = new CNContactStore ();
                    request.UpdateContact (mutableContact);
                    NSError error;
                    if (store.ExecuteSaveRequest (request, out error)) {
                        Console.WriteLine ("Contact updated");
                    } else {
                        Console.WriteLine ("Update error: {0}", error);
                    }

                }
            } else {
                //COPY APPSETTINGS INTO NEW CONTACT;
                CNMutableContact mutableContact = new CNMutableContact ();
                var server = (NSString)defaults.ValueForKey ((NSString)"server");
                var kvdt = ((NSNumber)defaults.ValueForKey ((NSString)"kvdt"));

                if (server == null) {
                    server = (NSString)"content.ing.ujambo.nl";
                }
                if (kvdt == null) {
                    kvdt = 1;
                }
                mutableContact.Nickname = server + " " + kvdt;
                mutableContact.GivenName = "configuration " + UIDevice.CurrentDevice.Name;
                CNContactStore store = new CNContactStore ();
                CNSaveRequest request = new CNSaveRequest ();
                request.AddContact (mutableContact, store.DefaultContainerIdentifier);
                NSError error;
                if (store.ExecuteSaveRequest (request, out error)) {
                    Console.WriteLine ("New contact saved");
                } else {
                    Console.WriteLine ("Save error: {0}", error);
                }
            }
            return true;
        }

        public override void OnResignActivation (UIApplication application)
        {
            // Invoked when the application is about to move from active to inactive state.
            // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
            // or when the user quits the application and it begins the transition to the background state.
            // Games should use this method to pause the game.
        }

        public override void DidEnterBackground (UIApplication application)
        {
            //ServiceContainer.Resolve<IBeaconService> ().startUpdatingLocationChanges ();
            // Use this method to release shared resources, save user data, invalidate timers and store the application state.
            // If your application supports background exection this method is called instead of WillTerminate when the user quits.
        }

        public override void WillEnterForeground (UIApplication application)
        {
            // Called as part of the transiton from background to active state.
            // Here you can undo many of the changes made on entering the background.
        }

        public override void OnActivated (UIApplication application)
        {
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
        }

        public override void WillTerminate (UIApplication application)
        {
            //ServiceContainer.Resolve<IBeaconService>().startMonitoringForSignificantLocationChanges();
            // Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
        }
    }
}

