using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;
using System.Threading.Tasks;
using System.Threading;
using CoreAnimation;
using System.Collections.Generic;
using AVFoundation;
using MediaPlayer;

namespace Enquete.iOS
{
    public enum GreenUp
    {
        down,
        up
    }

    partial class WebViewController : UIViewController
    {
        private UIWebView webView;

        private string url;
        private Diagnostics diagnostics;
        //        private bool isRefreshWebpageOn;
        private UIView screenSaver;
        private LoadingOverlay webLoadingOverlay;
        private IntPtr tokenObserveVolume = (IntPtr)1;
        private long startTime;
        private long expired;
        private const int secondsIdle = 120;
        private bool busyStartingOrDelayingScreensaver;

        private string domainServer;
        private bool isKVDT;
        private string domainTitle;
        private UIButton soundButton;
        private bool soundOn;
        private UISlider slider;
        public WebViewController (IntPtr handle)
            : base (handle)
        {

            this.busyStartingOrDelayingScreensaver = false;

            diagnostics = ServiceContainer.Resolve<Diagnostics> ();

        }

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear (animated);
            //            isRefreshWebpageOn = true;
            //            StartRefreshingPage();
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();
            this.domainServer = GetServerDomainFromSettings (false);
            this.isKVDT = GetKantoorVanDeToekomstSetting ();
            this.domainTitle = GetServerDomainFromSettings (true);
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
                if (isKVDT) {
                    url = "http://" + domainServer + "/kvdt/tablet/apple";

                } else {
                    url = "http://" + domainServer + "/appdemo/ipad";
                }
            } else {
                url = "http://" + domainServer + "/appdemo/iphone";
            }
            Console.WriteLine (url);

            this.SetNeedsStatusBarAppearanceUpdate ();
            this.webLoadingOverlay = new LoadingOverlay (View.Bounds, "De app is aan het laden...");
            this.webView = CreateWebview ();
            this.screenSaver = CreateScreensaver ();

            UIGestureRecognizer [] gestureRecognizers = CreateGestureRecognizers ();

            // verberg de screensaver
            this.HideScreensaver ();
            CreateSoundButton ();
            UILabel label = CreateDevelopmentInfoBar ();
            this.View.Add (webView);
            this.View.Add (webLoadingOverlay);
            this.View.Add (screenSaver);
            this.View.Add (label);
            this.View.Add (soundButton);
            if (domainTitle == "Production") {
                label.Hidden = true;
            }

            for (int i = 0; i < gestureRecognizers.Length; i++) {
                this.View.AddGestureRecognizer (gestureRecognizers [i]);
            }

            StartDelayedScreensaver ();
            UIApplication.SharedApplication.IdleTimerDisabled = false;
        }

        private UILabel CreateDevelopmentInfoBar ()
        {
            UILabel label = new UILabel (new CGRect (0, View.Bounds.Size.Height - 30, View.Bounds.Size.Width, 30));
            label.Text = "Env:" + domainTitle + " KVDT:" + isKVDT + " AppVersion:" + NSBundle.MainBundle.InfoDictionary ["CFBundleShortVersionString"];
            label.Hidden = false;
            return label;
        }

        private void CreateSoundButton ()
        {
            MPVolumeView volumeView = new MPVolumeView ();
            for (int i = 0; i < volumeView.Subviews.Length; i++) {
                if (volumeView.Subviews [i] is UISlider) {
                    slider = (UISlider)volumeView.Subviews [i];
                }
            }
            slider.ValueChanged += (sender, e) => {
                if (slider.Value > 0) {
                    UIImage image2 = UIImage.FromBundle ("btn_volume_aan.png");
                    soundButton.SetImage (image2, UIControlState.Normal);
                    soundButton.Frame = new CGRect (View.Bounds.Size.Width - (40 + image2.Size.Width * 2), 70, image2.Size.Width * 2, image2.Size.Height * 2);
                    soundOn = true;

                } else {
                    //turn sound off
                    UIImage image1 = UIImage.FromBundle ("btn_volume_uit.png");
                    soundButton.SetImage (image1, UIControlState.Normal);
                    soundButton.Frame = new CGRect (View.Bounds.Size.Width - (40 + image1.Size.Width * 2), 70, image1.Size.Width * 2, image1.Size.Height * 2);
                    soundOn = false;

                }
            };
            //slider.AddObserver (this, (NSString)"Value",
            //NSKeyValueObservingOptions.Old | NSKeyValueObservingOptions.New,
            //tokenObserveVolume);
            soundButton = new UIButton (UIButtonType.Custom);
            UIImage image = UIImage.FromBundle ("btn_volume_uit.png");
            soundButton.SetImage (image, UIControlState.Normal);

            soundButton.Frame = new CGRect (View.Bounds.Size.Width - (40 + image.Size.Width * 2), 70, image.Size.Width * 2, image.Size.Height * 2);
            this.soundOn = false;
            soundButton.TouchDown += (sender, e) => {
                if (soundOn) {
                    //turn sound off
                    slider.Value = 0;
                    UIImage image1 = UIImage.FromBundle ("btn_volume_uit.png");
                    soundButton.SetImage (image1, UIControlState.Normal);
                    soundButton.Frame = new CGRect (View.Bounds.Size.Width - (40 + image.Size.Width * 2), 70, image.Size.Width * 2, image.Size.Height * 2);
                    soundOn = false;
                } else {
                    //turn sound on
                    slider.Value = 1;
                    UIImage image2 = UIImage.FromBundle ("btn_volume_aan.png");
                    soundButton.SetImage (image2, UIControlState.Normal);
                    soundButton.Frame = new CGRect (View.Bounds.Size.Width - (40 + image.Size.Width * 2), 70, image.Size.Width * 2, image.Size.Height * 2);
                    soundOn = true;
                }
            };
            soundButton.Hidden = true;
        }

        private UIGestureRecognizer [] CreateGestureRecognizers ()
        {
            GestureDelegate gstrdelegate = new GestureDelegate ();
            Action action = () => {
                StartOrDelayScreensaver ();
            };

            UIGestureRecognizer [] gestureRecognizers = new UIGestureRecognizer [7];
            gestureRecognizers [0] = new UITapGestureRecognizer ();
            gestureRecognizers [0].AddTarget (action);
            gestureRecognizers [0].Delegate = gstrdelegate;
            gestureRecognizers [1] = new UIPinchGestureRecognizer ();
            gestureRecognizers [1].AddTarget (action);
            gestureRecognizers [1].Delegate = gstrdelegate;
            gestureRecognizers [2] = new UIRotationGestureRecognizer ();
            gestureRecognizers [2].AddTarget (action);
            gestureRecognizers [2].Delegate = gstrdelegate;
            gestureRecognizers [3] = new UISwipeGestureRecognizer ();
            gestureRecognizers [3].AddTarget (action);
            gestureRecognizers [3].Delegate = gstrdelegate;
            gestureRecognizers [4] = new UIPanGestureRecognizer ();
            gestureRecognizers [4].AddTarget (action);
            gestureRecognizers [4].Delegate = gstrdelegate;
            gestureRecognizers [5] = new UIScreenEdgePanGestureRecognizer ();
            gestureRecognizers [5].AddTarget (action);
            gestureRecognizers [5].Delegate = gstrdelegate;
            gestureRecognizers [6] = new UILongPressGestureRecognizer ();
            gestureRecognizers [6].AddTarget (action);
            gestureRecognizers [6].Delegate = gstrdelegate;
            return gestureRecognizers;
        }

        private UIWebView CreateWebview ()
        {
            UIWebView wView = new UIWebView (new CGRect (0, 0, View.Bounds.Size.Width, View.Bounds.Size.Height));
            wView.AllowsInlineMediaPlayback = true;
            wView.MediaPlaybackRequiresUserAction = false;
            wView.LoadFinished += HandleLoadFinished;
            wView.ScrollView.Bounces = false;
            NSUrlRequest urlRequest = new NSUrlRequest (new NSUrl (url));

            wView.LoadRequest (urlRequest);
            wView.ShouldStartLoad += (theView, request, navType) => {
                var absStr = ((NSUrlRequest)request).Url.AbsoluteString;

                soundButton.Hidden = true;
                if (absStr.Contains ("videowall") || absStr.Contains ("vimeo")) {
                    Console.WriteLine ("Videowall");
                    soundButton.Hidden = false;
                } else {
                    Console.WriteLine (absStr);
                }
                return true;
            };

            return wView;
        }

        private UIView CreateScreensaver ()
        {

            UIImageView sSaver = new UIImageView (new CGRect (0, 0, View.Bounds.Size.Width, View.Bounds.Size.Height));
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
                sSaver.Image = UIImage.FromBundle ("iPadAir2_screensaver (2048x1536)");
            } else {
                sSaver.Image = UIImage.FromBundle ("iPhone6_screensaver (750x1334)");
            }

            return sSaver;
        }

        private async void StartDelayedScreensaver ()
        {
            while (webView.IsLoading) {
                Console.WriteLine ("Loading");
                await Task.Delay (100);
            }

            startTime = DateTime.UtcNow.Ticks;
            this.expired = DateTime.UtcNow.AddSeconds (secondsIdle).Ticks;
            while (!IsTimerExpired ()) {
                await Task.Delay (5000);

                Console.WriteLine ("Delay");
            }

            SendActivity ();
            webLoadingOverlay.Hidden = false;
            NSUrlCache cache = new NSUrlCache ();
            cache.RemoveAllCachedResponses ();
            webView.LoadRequest (new NSUrlRequest (new NSUrl (url)));
            Console.WriteLine ("start screensaver");
            this.screenSaver.Hidden = false;
        }

        private void HideScreensaver ()
        {
            this.screenSaver.Hidden = true;
        }


        /// <summary>
        /// todo
        /// </summary>
        private void SendActivity ()
        {
            CancellationTokenSource tokenSource = new CancellationTokenSource ();
            DateTime startDate = new DateTime (startTime).AddSeconds (secondsIdle);
            TimeSpan timeSpan = new TimeSpan (expired - startDate.Ticks);
            //diagnostics.Send (tokenSource.Token, new StatRecord (new Details (UIDevice.CurrentDevice.SystemVersion), timeSpan.TotalSeconds), "dev.content.ing.ujambo.nl");
            //send is terminated, wait for fix

            var statRecord = new StatRecord (new Details (UIDevice.CurrentDevice.SystemVersion), timeSpan.TotalSeconds);
            if (domainServer != "") {
                Console.WriteLine ("Current server = " + domainServer);
                bool isADVISEUR = false;
                diagnostics.Send (tokenSource.Token, statRecord, domainServer, isKVDT, isADVISEUR);
            }
            //var plist = NSUserDefaults.StandardUserDefaults;
            //if (arrTime != null)
            //{
            //    //plist.
            //    if (arrTime != null)
            //    {                  
            //        if (value != null)
            //        {
            //            Console.WriteLine("Daar gaat iets goed");
            //        }
            //        else
            //        {
            //            Console.WriteLine("value is null");
            //        }
            //        Console.WriteLine(plist.);
            //        GetServerDomainFromSettings
            //    }
            //    else
            //    {
            //        Console.WriteLine("key is null");
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("Array is null");
            //}
        }
        /// <summary>
        /// true returns title, false returns value
        /// </summary>
        /// <returns>The server domain from settings.</returns>
        /// <param name="isValue">Is value.</param>
        public string GetServerDomainFromSettings (Boolean returnTitle)
        {
            var settingsDict = new NSDictionary (NSBundle.MainBundle.PathForResource ("Settings.bundle/Root.plist", null));
            string result = "";


            NSUserDefaults defaults = NSUserDefaults.StandardUserDefaults;
            if (settingsDict != null) {
                NSArray prefSpecifierDictionary = settingsDict ["PreferenceSpecifiers"] as NSArray;


                foreach (var prefItem in NSArray.FromArray<NSDictionary> (prefSpecifierDictionary)) {

                    var key = (NSString)prefItem ["Key"];
                    if (key == null) {
                        continue;
                    }
                    if (key.ToString () == "server") {
                        var domainName = defaults.ValueForKey (key);
                        if (domainName == null) {
                            domainName = new NSString ("content.ing.ujambo.nl");
                        }
                        result = (NSString)domainName;
                        if (returnTitle) {
                            try {
                                var titles = prefItem ["Titles"] as NSArray;
                                var values = prefItem ["Values"] as NSArray;
                                result = titles?.GetItem<NSString> (values.IndexOf (domainName)).ToString ();
                            } catch (Exception e) {

                            }
                        }
                    }
                }
            }

            return result;
        }

        private bool GetKantoorVanDeToekomstSetting ()
        {

            var settingsDict = new NSDictionary (NSBundle.MainBundle.PathForResource ("Settings.bundle/Root.plist", null));
            bool result = true;
            NSUserDefaults defaults = NSUserDefaults.StandardUserDefaults;
            if (settingsDict != null) {
                NSArray prefSpecifierDictionary = settingsDict ["PreferenceSpecifiers"] as NSArray;


                foreach (var prefItem in NSArray.FromArray<NSDictionary> (prefSpecifierDictionary)) {

                    var key = (NSString)prefItem ["Key"];
                    if (key == null) {
                        continue;
                    }
                    if (key.ToString () == "kvdt") {
                        NSNumber kVDTSetting = (NSNumber)defaults.ValueForKey (key);
                        result = true;
                        if (kVDTSetting != null) {
                            if (kVDTSetting.ToString () == "0") {
                                result = false;
                            }
                        }

                    }
                }
            }
            Console.WriteLine (result);
            return result;
        }
        public bool IsTimerExpired ()
        {
            bool isExpired = false;
            DateTime currentTime = DateTime.UtcNow;
            if (this.expired < currentTime.Ticks) {
                isExpired = true;
            }

            return isExpired;
        }

        public void DelayScreensaver ()
        {
            DateTime currentTime = DateTime.UtcNow;
            this.expired = currentTime.AddSeconds (secondsIdle).Ticks;
        }


        [Export ("StartOrDelayScreensaver")]
        private async void StartOrDelayScreensaver ()
        {
            if (!busyStartingOrDelayingScreensaver) {
                busyStartingOrDelayingScreensaver = true;
                if (!this.screenSaver.Hidden) {
                    HideScreensaver ();
                    StartDelayedScreensaver ();
                } else {
                    DelayScreensaver ();
                }
                busyStartingOrDelayingScreensaver = false;
            }
        }

        private async void HandleLoadFinished (Object sender, EventArgs e)
        {
            Console.WriteLine ("loadfinished");
            while (webView.IsLoading) {
                await Task.Delay (100);
            }
            webLoadingOverlay.Hidden = true;
        }
        //        private async void StartRefreshingPage()
        //        {
        //            while (isRefreshWebpageOn)
        //            {
        //                await Task.Delay(600 * 1000);
        //                if (DateTime.UtcNow.ToLocalTime().TimeOfDay.Hours >= 6 && DateTime.UtcNow.ToLocalTime().TimeOfDay.Hours < 7)
        //                {
        //                    webView.Reload();
        //                    NSUrlCache cache = new NSUrlCache();
        //                    cache.RemoveAllCachedResponses();
        //                }
        //            }
        //        }


        // delegate methods for UITapGestureRecognizer
        public class GestureDelegate : UIGestureRecognizerDelegate
        {
            public override bool ShouldReceiveTouch (UIGestureRecognizer recognizer, UITouch touch)
            {
                return true;
            }

            public override bool ShouldBegin (UIGestureRecognizer recognizer)
            {
                return true;
            }

            public override bool ShouldRecognizeSimultaneously (UIGestureRecognizer gestureRecognizer, UIGestureRecognizer otherGestureRecognizer)
            {
                return true;
            }

            public override bool ShouldReceivePress (UIGestureRecognizer gestureRecognizer, UIPress press)
            {
                return true;
            }
        }

        public override UIStatusBarStyle PreferredStatusBarStyle ()
        {
            if (UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
                return UIStatusBarStyle.LightContent;
            }
            return UIStatusBarStyle.Default;
        }
    }
}
