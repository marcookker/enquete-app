using System;
using UIKit;

namespace Enquete.iOS
{
    public class ColorConverter
    {
        public ColorConverter()
        {
        }

        public static UIColor StringToUIColor(string hex)
        {
            string[] hexValuesSplit = hex.Split(' ');
            Int32 b = Convert.ToInt32(hexValuesSplit[0], 16);
            Int32 g = Convert.ToInt32(hexValuesSplit[1], 16);
            Int32 r = Convert.ToInt32(hexValuesSplit[2], 16);
            return UIColor.FromRGB(b, g, r);
        }
    }
}

