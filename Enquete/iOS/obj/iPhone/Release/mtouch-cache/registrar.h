#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wtypedef-redefinition"
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"
#include <stdarg.h>
#include <xamarin/xamarin.h>
#include <objc/objc.h>
#include <objc/runtime.h>
#include <objc/message.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CAEmitterBehavior.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreImage/CoreImage.h>
#import <PassKit/PassKit.h>
#import <GameKit/GameKit.h>
#import <Photos/Photos.h>
#import <UserNotifications/UserNotifications.h>
#import <MapKit/MapKit.h>
#import <GameplayKit/GameplayKit.h>
#import <VideoSubscriberAccount/VideoSubscriberAccount.h>
#import <AVKit/AVKit.h>
#import <WatchKit/WatchKit.h>
#import <CoreLocation/CoreLocation.h>
#import <WebKit/WebKit.h>
#import <Accounts/Accounts.h>
#import <AdSupport/AdSupport.h>
#import <AddressBookUI/AddressBookUI.h>
#import <MediaPlayer/MediaPlayer.h>
#import <PushKit/PushKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ReplayKit/ReplayKit.h>
#import <HealthKit/HealthKit.h>
#import <CoreMIDI/CoreMIDI.h>
#import <SafariServices/SafariServices.h>
#import <CoreMotion/CoreMotion.h>
#import <SceneKit/SceneKit.h>
#import <AudioUnit/AudioUnit.h>
#import <CallKit/CallKit.h>
#import <CoreSpotlight/CoreSpotlight.h>
#import <CloudKit/CloudKit.h>
#import <Messages/Messages.h>
#import <Metal/Metal.h>
#import <SpriteKit/SpriteKit.h>
#import <HomeKit/HomeKit.h>
#import <CoreTelephony/CoreTelephonyDefines.h>
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTSubscriber.h>
#import <CoreTelephony/CTSubscriberInfo.h>
#import <Contacts/Contacts.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreData/CoreData.h>
#import <ContactsUI/ContactsUI.h>
#import <MetalPerformanceShaders/MetalPerformanceShaders.h>
#import <EventKit/EventKit.h>
#import <MetalKit/MetalKit.h>
#import <GLKit/GLKit.h>
#import <GameController/GameController.h>
#import <Intents/Intents.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import <ExternalAccessory/ExternalAccessory.h>
#import <ModelIO/ModelIO.h>
#import <MessageUI/MessageUI.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import <NetworkExtension/NetworkExtension.h>
#import <CoreAudioKit/CoreAudioKit.h>
#import <Social/Social.h>
#import <StoreKit/StoreKit.h>
#import <Twitter/Twitter.h>
#import <Speech/Speech.h>
#import <NewsstandKit/NewsstandKit.h>
#import <NotificationCenter/NotificationCenter.h>
#import <WatchConnectivity/WatchConnectivity.h>
#import <PhotosUI/PhotosUI.h>
#import <QuickLook/QuickLook.h>
#import <HealthKitUI/HealthKitUI.h>
#import <EventKitUI/EventKitUI.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <iAd/iAd.h>
#import <CoreGraphics/CoreGraphics.h>

@protocol CALayerDelegate;
@class CoreImage_CITileFilter;
@class CoreImage_CIParallelogramTile;
@class CoreImage_CICodeGenerator;
@class CoreImage_CIPdf417BarcodeGenerator;
@class CoreImage_CIPerspectiveTransform;
@class CoreImage_CIPerspectiveCorrection;
@class CoreImage_CIPerspectiveTile;
@class CoreImage_CIPerspectiveTransformWithExtent;
@class CoreImage_CIPhotoEffect;
@class CoreImage_CIPhotoEffectChrome;
@class CoreImage_CIPhotoEffectFade;
@class CoreImage_CIPhotoEffectInstant;
@class CoreImage_CIPhotoEffectMono;
@class CoreImage_CIPhotoEffectNoir;
@class CoreImage_CIPhotoEffectProcess;
@class CoreImage_CIPhotoEffectTonal;
@class CoreImage_CIPhotoEffectTransfer;
@class CoreImage_CIBlendFilter;
@class CoreImage_CIPinLightBlendMode;
@class CoreImage_CIDistortionFilter;
@class CoreImage_CIPinchDistortion;
@class CoreImage_CIPixellate;
@class CoreImage_CIPointillize;
@class CoreImage_CIQRCodeGenerator;
@class CoreImage_CIRadialGradient;
@class CoreImage_CIRandomGenerator;
@class __MonoTouch_UIVideoStatusDispatcher;
@class CoreImage_CITransitionFilter;
@class CoreImage_CIRippleTransition;
@class CoreImage_CIRowAverage;
@class CoreImage_CISRGBToneCurveToLinear;
@class CoreImage_CISaturationBlendMode;
@class CoreImage_CIScreenBlendMode;
@class CoreImage_CIScreenFilter;
@class CoreImage_CISepiaTone;
@class CoreImage_CIShadedMaterial;
@class CoreImage_CISharpenLuminance;
@class CoreImage_CISixfoldReflectedTile;
@class CoreImage_CISixfoldRotatedTile;
@class CoreImage_CILinearGradient;
@class CoreImage_CISmoothLinearGradient;
@class CoreImage_CISoftLightBlendMode;
@class CoreImage_CICompositingFilter;
@class CoreImage_CISourceAtopCompositing;
@class CoreImage_CISourceInCompositing;
@class CoreImage_CISourceOutCompositing;
@class CoreImage_CISourceOverCompositing;
@class CoreImage_CISpotColor;
@class CoreImage_CISpotLight;
@class CoreImage_CIStarShineGenerator;
@class CoreImage_CIStraightenFilter;
@class CoreImage_CIStretchCrop;
@class CoreImage_CIStripesGenerator;
@class CoreImage_CISubtractBlendMode;
@class CoreImage_CISunbeamsGenerator;
@class CoreImage_CISwipeTransition;
@class CoreImage_CITemperatureAndTint;
@class CoreImage_CIThermal;
@class CoreImage_CIToneCurve;
@class CoreImage_CITorusLensDistortion;
@class CoreImage_CITriangleKaleidoscope;
@class CoreImage_CITriangleTile;
@class CoreImage_CITwelvefoldReflectedTile;
@class CoreImage_CITwirlDistortion;
@class CoreImage_CIUnsharpMask;
@class CoreImage_CIVibrance;
@class CoreImage_CIVignette;
@class CoreImage_CIVignetteEffect;
@class CoreImage_CIVortexDistortion;
@class CoreImage_CIWhitePointAdjust;
@class CoreImage_CIXRay;
@class CoreImage_CIZoomBlur;
@class AVFoundation_InternalAVAudioPlayerDelegate;
@class AVFoundation_InternalAVAudioRecorderDelegate;
@class AVFoundation_InternalAVAudioSessionDelegate;
@class __MonoMac_NSActionDispatcher;
@class __MonoMac_ActionDispatcher;
@class __Xamarin_NSTimerActionDispatcher;
@class __MonoMac_NSAsyncActionDispatcher;
@class Foundation_InternalNSNotificationHandler;
@class GameKit_Mono_GKSessionDelegate;
@protocol UIPickerViewModel;
@class AddressBookUI_InternalABNewPersonViewControllerDelegate;
@class AddressBookUI_InternalABPeoplePickerNavigationControllerDelegate;
@class AddressBookUI_InternalABPersonViewControllerDelegate;
@class AddressBookUI_InternalABUnknownPersonViewControllerDelegate;
@class MessageUI_Mono_MFMailComposeViewControllerDelegate;
@class MessageUI_Mono_MFMessageComposeViewControllerDelegate;
@class UIKit_UIControlEventProxy;
@class __MonoTouch_UIImageStatusDispatcher;
@class CoreImage_CIAccordionFoldTransition;
@class CoreImage_CIAdditionCompositing;
@class CoreImage_CIAffineFilter;
@class CoreImage_CIAffineClamp;
@class CoreImage_CIAffineTile;
@class CoreImage_CIAffineTransform;
@class CoreImage_CIAreaAverage;
@class CoreImage_CIAreaHistogram;
@class CoreImage_CIAreaMaximum;
@class CoreImage_CIAreaMaximumAlpha;
@class CoreImage_CIAreaMinimum;
@class CoreImage_CIAreaMinimumAlpha;
@class CoreImage_CIAztecCodeGenerator;
@class CoreImage_CIBarsSwipeTransition;
@class CoreImage_CIBlendWithMask;
@class CoreImage_CIBlendWithAlphaMask;
@class CoreImage_CIBloom;
@class CoreImage_CIBoxBlur;
@class CoreImage_CIBumpDistortion;
@class CoreImage_CIBumpDistortionLinear;
@class CoreImage_CICheckerboardGenerator;
@class CoreImage_CICircleSplashDistortion;
@class CoreImage_CICircularScreen;
@class CoreImage_CICircularWrap;
@class CoreImage_CIClamp;
@class CoreImage_CICmykHalftone;
@class CoreImage_CICode128BarcodeGenerator;
@class CoreImage_CIColorBlendMode;
@class CoreImage_CIColorBurnBlendMode;
@class CoreImage_CIColorClamp;
@class CoreImage_CIColorControls;
@class CoreImage_CIColorCrossPolynomial;
@class CoreImage_CIColorCube;
@class CoreImage_CIColorCubeWithColorSpace;
@class CoreImage_CIColorDodgeBlendMode;
@class CoreImage_CIColorInvert;
@class CoreImage_CIColorMap;
@class CoreImage_CIColorMatrix;
@class CoreImage_CIColorMonochrome;
@class CoreImage_CIColorPolynomial;
@class CoreImage_CIColorPosterize;
@class CoreImage_CIColumnAverage;
@class CoreImage_CIComicEffect;
@class CoreImage_CIConstantColorGenerator;
@class CoreImage_CIConvolutionCore;
@class CoreImage_CIConvolution3X3;
@class CoreImage_CIConvolution5X5;
@class CoreImage_CIConvolution7X7;
@class CoreImage_CIConvolution9Horizontal;
@class CoreImage_CIConvolution9Vertical;
@class CoreImage_CICopyMachineTransition;
@class CoreImage_CICrop;
@class CoreImage_CICrystallize;
@class CoreImage_CIDarkenBlendMode;
@class CoreImage_CIEdgeWork;
@class CoreImage_CIDepthOfField;
@class CoreImage_CIEdges;
@class CoreImage_CIEightfoldReflectedTile;
@class CoreImage_CIDifferenceBlendMode;
@class CoreImage_CIExclusionBlendMode;
@class CoreImage_CIExposureAdjust;
@class CoreImage_CIDiscBlur;
@class CoreImage_CIFaceBalance;
@class CoreImage_CIDisintegrateWithMaskTransition;
@class CoreImage_CIDisplacementDistortion;
@class CoreImage_CIDissolveTransition;
@class CoreImage_CIFalseColor;
@class CoreImage_CIDivideBlendMode;
@class CoreImage_CIGlassLozenge;
@class CoreImage_CIDotScreen;
@class CoreImage_CIGlideReflectedTile;
@class CoreImage_CIGloom;
@class CoreImage_CIDroste;
@class CoreImage_CIHardLightBlendMode;
@class CoreImage_CIHatchedScreen;
@class CoreImage_CIHeightFieldFromMask;
@class CoreImage_CIHexagonalPixellate;
@class CoreImage_CIHighlightShadowAdjust;
@class CoreImage_CIHistogramDisplayFilter;
@protocol CIImageProvider;
@class CoreImage_CIHoleDistortion;
@class CoreImage_CIHueAdjust;
@class CoreImage_CIKaleidoscope;
@class CoreImage_CIHueBlendMode;
@class CoreImage_CIHueSaturationValueGradient;
@class CoreImage_CILanczosScaleTransform;
@class CoreImage_CILenticularHaloGenerator;
@class CoreImage_CILightTunnel;
@class CoreImage_CIFlashTransition;
@class CoreImage_CILightenBlendMode;
@class CoreImage_CILuminosityBlendMode;
@class CoreImage_CIFourfoldReflectedTile;
@class CoreImage_CILineOverlay;
@class CoreImage_CIMaskToAlpha;
@class CoreImage_CILineScreen;
@class CoreImage_CILinearBurnBlendMode;
@class CoreImage_CILinearDodgeBlendMode;
@class CoreImage_CIMaskedVariableBlur;
@class CoreImage_CILinearToSRGBToneCurve;
@class CoreImage_CIOverlayBlendMode;
@class CoreImage_CIFourfoldRotatedTile;
@class CoreImage_CIPageCurlTransition;
@class CoreImage_CIPageCurlWithShadowTransition;
@class CoreImage_CIMaximumComponent;
@class CoreImage_CIMaximumCompositing;
@class CoreImage_CIMedianFilter;
@class CoreImage_CIFourfoldTranslatedTile;
@class CoreImage_CIMinimumComponent;
@class CoreImage_CIMinimumCompositing;
@class CoreImage_CIGammaAdjust;
@class CoreImage_CIModTransition;
@class CoreImage_CIMotionBlur;
@class CoreImage_CIMultiplyBlendMode;
@class CoreImage_CIMultiplyCompositing;
@class CoreImage_CIGaussianBlur;
@class CoreImage_CINinePartStretched;
@class CoreImage_CIGaussianGradient;
@class CoreImage_CINinePartTiled;
@class CoreImage_CIGlassDistortion;
@class CoreImage_CINoiseReduction;
@class CoreImage_CIOpTile;
@protocol UIAccessibilityContainer;
@protocol UICollectionViewSource;
@class AppDelegate;
@class Enquete_iOS_LoadingOverlay;
@class Enquete_iOS_WebViewController_GestureDelegate;
@class WebViewController;
@class UIKit_UIView_UIViewAppearance;
@class UIKit_UIScrollView_UIScrollViewAppearance;
@class UIKit_UITableView_UITableViewAppearance;
@class AVFoundation_AVSpeechSynthesizer__AVSpeechSynthesizerDelegate;
@class UIKit_UITableViewCell_UITableViewCellAppearance;
@class UIKit_UITextField__UITextFieldDelegate;
@class UIKit_UIControl_UIControlAppearance;
@class UIKit_UITextField_UITextFieldAppearance;
@class UIKit_UIScrollView__UIScrollViewDelegate;
@class UIKit_UITextView__UITextViewDelegate;
@class UIKit_UITextView_UITextViewAppearance;
@class UIKit_UIToolbar_UIToolbarAppearance;
@class MapKit_MKMapView__MKMapViewDelegate;
@class MapKit_MKMapView_MKMapViewAppearance;
@class UIKit_UIView__UIViewStaticCallback;
@class GameKit_GKTurnBasedMatchmakerViewController_GKTurnBasedMatchmakerViewControllerAppearance;
@class MapKit_MKOverlayView_MKOverlayViewAppearance;
@class MapKit_MKOverlayPathView_MKOverlayPathViewAppearance;
@class MapKit_MKAnnotationView_MKAnnotationViewAppearance;
@class MapKit_MKPinAnnotationView_MKPinAnnotationViewAppearance;
@class UIKit_UIGestureRecognizer__UIGestureRecognizerDelegate;
@class __UIGestureRecognizerToken;
@class __UIGestureRecognizerParameterlessToken;
@class __UIGestureRecognizerParametrizedToken;
@class MapKit_MKPolygonView_MKPolygonViewAppearance;
@class MapKit_MKPolylineView_MKPolylineViewAppearance;
@class AVFoundation_AVCaptureFileOutput_recordingProxy;
@class AddressBookUI_ABPeoplePickerNavigationController_ABPeoplePickerNavigationControllerAppearance;
@class UIKit_UIBarItem_UIBarItemAppearance;
@class UIKit_UIBarButtonItem_UIBarButtonItemAppearance;
@class MapKit_MKUserTrackingBarButtonItem_MKUserTrackingBarButtonItemAppearance;
@class Photos_PHPhotoLibrary___phlib_observer;
@class PhotosUI_PHLivePhotoView_PHLivePhotoViewAppearance;
@class UIKit_UIImagePickerController__UIImagePickerControllerDelegate;
@class CoreLocation_CLLocationManager__CLLocationManagerDelegate;
@class QuickLook_QLPreviewController__QLPreviewControllerDelegate;
@class UIKit_UIImageView_UIImageViewAppearance;
@class UIKit_UIInputView_UIInputViewAppearance;
@class MediaPlayer_MPMediaPickerController__MPMediaPickerControllerDelegate;
@class UIKit_UILabel_UILabelAppearance;
@class __UILongPressGestureRecognizer;
@class UIKit_UINavigationBar_UINavigationBarAppearance;
@class __NSObject_Disposer;
@class __XamarinObjectObserver;
@class Foundation_NSKeyedArchiver__NSKeyedArchiverDelegate;
@class Foundation_NSKeyedUnarchiver__NSKeyedUnarchiverDelegate;
@class Foundation_NSNetService__NSNetServiceDelegate;
@class MediaPlayer_MPVolumeView_MPVolumeViewAppearance;
@class UIKit_UIPageControl_UIPageControlAppearance;
@class MessageUI_MFMailComposeViewController_MFMailComposeViewControllerAppearance;
@class UIKit_UIPageViewController__UIPageViewControllerDelegate;
@class UIKit_UIPageViewController__UIPageViewControllerDataSource;
@class MessageUI_MFMessageComposeViewController_MFMessageComposeViewControllerAppearance;
@class Foundation_NSStream__NSStreamDelegate;
@class __UIPanGestureRecognizer;
@class Messages_MSStickerBrowserView_MSStickerBrowserViewAppearance;
@class UIKit_UIPickerView_UIPickerViewAppearance;
@class Messages_MSStickerView_MSStickerViewAppearance;
@class MonoTouch_GKSession_ReceivedObject;
@class GameKit_GKMatch__GKMatchDelegate;
@class __UIPinchGestureRecognizer;
@class UIKit_UIPopoverBackgroundView_UIPopoverBackgroundViewAppearance;
@class UIKit_UIPopoverController__UIPopoverControllerDelegate;
@class UIKit_UIPopoverPresentationController__UIPopoverPresentationControllerDelegate;
@class HealthKitUI_HKActivityRingView_HKActivityRingViewAppearance;
@class HomeKit_HMAccessory__HMAccessoryDelegate;
@class HomeKit_HMAccessoryBrowser__HMAccessoryBrowserDelegate;
@class UIKit_UIPreviewInteraction__UIPreviewInteractionDelegate;
@class SceneKit_SCNPhysicsWorld__SCNPhysicsContactDelegate;
@class UIKit_UIPrintInteractionController__UIPrintInteractionControllerDelegate;
@class CoreAnimation_CAAnimation__CAAnimationDelegate;
@class CoreBluetooth_CBCentralManager__CBCentralManagerDelegate;
@class CoreBluetooth_CBPeripheralManager__CBPeripheralManagerDelegate;
@class CoreBluetooth_CBPeripheral__CBPeripheralDelegate;
@class UIKit_UIProgressView_UIProgressViewAppearance;
@class HomeKit_HMCameraView_HMCameraViewAppearance;
@class UIKit_UIRefreshControl_UIRefreshControlAppearance;
@class __UIRotationGestureRecognizer;
@class __UIScreenEdgePanGestureRecognizer;
@class MetalKit_MTKView_MTKViewAppearance;
@class UIKit_UISearchBar__UISearchBarDelegate;
@class UIKit_UISearchBar_UISearchBarAppearance;
@class UIKit_UISearchController___Xamarin_UISearchResultsUpdating;
@class SceneKit_SCNView_SCNViewAppearance;
@class EventKitUI_EKCalendarChooser__EKCalendarChooserDelegate;
@class EventKitUI_EKEventEditViewController__EKEventEditViewDelegate;
@class EventKitUI_EKEventEditViewController_EKEventEditViewControllerAppearance;
@class EventKitUI_EKEventViewController__EKEventViewDelegate;
@class HomeKit_HMHome__HMHomeDelegate;
@class ExternalAccessory_EAAccessory__EAAccessoryDelegate;
@class ExternalAccessory_EAWiFiUnconfiguredAccessoryBrowser__EAWiFiUnconfiguredAccessoryBrowserDelegate;
@class UIKit_UISegmentedControl_UISegmentedControlAppearance;
@class HomeKit_HMHomeManager__HMHomeManagerDelegate;
@class CoreAudioKit_CAInterAppAudioSwitcherView_CAInterAppAudioSwitcherViewAppearance;
@class CoreAudioKit_CAInterAppAudioTransportView_CAInterAppAudioTransportViewAppearance;
@class Foundation_NSCache__NSCacheDelegate;
@class UIKit_UISlider_UISliderAppearance;
@class __MonoMac_FuncBoolDispatcher;
@class UIKit_UIActionSheet__UIActionSheetDelegate;
@class UIKit_UIActionSheet_UIActionSheetAppearance;
@class UIKit_UIDocumentMenuViewController__UIDocumentMenuDelegate;
@class UIKit_UIDocumentPickerViewController__UIDocumentPickerDelegate;
@class UIKit_UIAlertView__UIAlertViewDelegate;
@class UIKit_UIAlertView_UIAlertViewAppearance;
@class UIKit_UIBarButtonItem_Callback;
@class UIKit_UISplitViewController__UISplitViewControllerDelegate;
@class UIKit_UIStackView_UIStackViewAppearance;
@class UIKit_UIButton_UIButtonAppearance;
@class UIKit_UICollectionView_UICollectionViewAppearance;
@class UIKit_UIStepper_UIStepperAppearance;
@class __UITapGestureRecognizer;
@class __UISwipeGestureRecognizer;
@class UIKit_UISwitch_UISwitchAppearance;
@class UIKit_UITabBar__UITabBarDelegate;
@class UIKit_UITabBar_UITabBarAppearance;
@class UIKit_UITabBarController__UITabBarControllerDelegate;
@class UIKit_UITabBarItem_UITabBarItemAppearance;
@class SpriteKit_SKPhysicsWorld__SKPhysicsContactDelegate;
@class UIKit_UITableViewHeaderFooterView_UITableViewHeaderFooterViewAppearance;
@class SpriteKit_SKView_SKViewAppearance;
@class StoreKit_SKRequest__SKRequestDelegate;
@class StoreKit_SKProductsRequest__SKProductsRequestDelegate;
@class StoreKit_SKStoreProductViewController__SKStoreProductViewControllerDelegate;
@class Foundation_NSMetadataQuery__NSMetadataQueryDelegate;
@class Foundation_NSNetServiceBrowser__NSNetServiceBrowserDelegate;
@class UIKit_UIVideoEditorController__UIVideoEditorControllerDelegate;
@class UIKit_NSTextStorage__NSTextStorageDelegate;
@class UIKit_UIAccelerometer__UIAccelerometerDelegate;
@class GameKit_GKGameCenterViewController__GKGameCenterControllerDelegate;
@class GameKit_GKAchievementViewController__GKAchievementViewControllerDelegate;
@class GameKit_GKAchievementViewController_GKAchievementViewControllerAppearance;
@class GameKit_GKChallengeEventHandler__GKChallengeEventHandlerDelegate;
@class GameKit_GKFriendRequestComposeViewController__GKFriendRequestComposeViewControllerDelegate;
@class GameKit_GKFriendRequestComposeViewController_GKFriendRequestComposeViewControllerAppearance;
@class UIKit_UIActivityIndicatorView_UIActivityIndicatorViewAppearance;
@class GLKit_GLKView__GLKViewDelegate;
@class GLKit_GLKView_GLKViewAppearance;
@class GameKit_GKMatchmakerViewController__GKMatchmakerViewControllerDelegate;
@class PassKit_PKAddPassButton_PKAddPassButtonAppearance;
@class PassKit_PKAddPassesViewController__PKAddPassesViewControllerDelegate;
@class MapKit_MKCircleView_MKCircleViewAppearance;
@class GameKit_GKLeaderboardViewController__GKLeaderboardViewControllerDelegate;
@class GameKit_GKLeaderboardViewController_GKLeaderboardViewControllerAppearance;
@class UIKit_UICollectionReusableView_UICollectionReusableViewAppearance;
@class UIKit_UICollectionViewCell_UICollectionViewCellAppearance;
@class PassKit_PKPaymentAuthorizationViewController__PKPaymentAuthorizationViewControllerDelegate;
@class UIKit_UIDatePicker_UIDatePickerAppearance;
@class UIKit_UICollisionBehavior__UICollisionBehaviorDelegate;
@class PassKit_PKPaymentButton_PKPaymentButtonAppearance;
@class UIKit_UIVisualEffectView_UIVisualEffectViewAppearance;
@class UIKit_UIDocumentInteractionController__UIDocumentInteractionControllerDelegate;
@class UIKit_UIWebView__UIWebViewDelegate;
@class UIKit_UIWebView_UIWebViewAppearance;
@class UIKit_UIWindow_UIWindowAppearance;
@class iAd_ADBannerView__ADBannerViewDelegate;
@class iAd_ADBannerView_ADBannerViewAppearance;
@class WebKit_WKWebView_WKWebViewAppearance;
@class iAd_ADInterstitialAd__ADInterstitialAdDelegate;
@class System_Net_Http_NSUrlSessionHandler_DataTaskDelegate;

