using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Net;
using System.Threading;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace Enquete
{

    public class Diagnostics
    {
        private EnqueteHttpService httpService;
        private string devicePart;
        private Device device;
        public Diagnostics (EnqueteHttpService httpService, Device device)
        {
            this.httpService = httpService;
            this.device = device;
        }

        async public void Send (CancellationToken token, StatRecord s, string domainPart, bool isKVDT, bool isADVISEUR)
        {
            try {
                string data = "usage=" + JsonConvert.SerializeObject (s);
                string httpPart = "http://";
                string statsPart = "/stats/incoming";
                devicePart = "/";

                if (isADVISEUR) {
                    devicePart = devicePart + "adviseur_";
                }

                if (isKVDT && !isADVISEUR) {
                    devicePart = devicePart + "kvdt_";
                }

                devicePart = devicePart + Enum.GetName (typeof (Device), device);

                Debug.WriteLine ("data: " + data + " url: " + httpPart + domainPart + statsPart + devicePart);
                await httpService.PostAsync (token, data, httpPart + domainPart + statsPart + devicePart);
            } catch (Exception e) {
                Debug.WriteLine ("send diagnostics error: " + e.Message);
            }
        }

    }



    [DataContract]
    public class StatRecord
    {
        public StatRecord (Details details, double duration)
        {
            this.details = JsonConvert.SerializeObject (details);
            this.duration = duration;
        }
        [DataMember]
        public double duration { get; set; }
        [DataMember]
        public string details { get; set; }
    }

    [DataContract]
    public class Details
    {
        [DataMember]
        public string deviceVersion { get; set; }
        [DataMember]
        public string elementClicks { get; set; }

        public Details (string deviceVersion)
        {
            this.deviceVersion = deviceVersion;
        }
        public Details (string deviceVersion, KeyValuePair<string, int> [] elementClicks)
        {
            this.deviceVersion = deviceVersion;
            this.elementClicks = MyDictionaryToJson (elementClicks);

        }

        private string MyDictionaryToJson (KeyValuePair<string, int> [] dict)
        {
            var entries = dict.Select (d =>
                 string.Format ("\"{0}\": [{1}]", d.Key, string.Join (",", d.Value)));
            return "{" + string.Join (",", entries) + "}";
        }

    }


}

