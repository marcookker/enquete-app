using System;

namespace Enquete
{
    public enum Device
    {
        ipad,
        iphone,
        android,
        samsung,
        samsung_tab,
        tab
    }
}

