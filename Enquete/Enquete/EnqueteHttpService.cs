using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Security;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace Enquete
{
    public class EnqueteHttpService
    {
        private HttpClient myClient;
        public static string NOT_CONNECTED_PHRASE = "NotConnected";

        public EnqueteHttpService()
        {
            myClient = new HttpClient();
        }

        public async Task<HttpResponseMessage> PostAsync(CancellationToken token, string resource, string url)
        {
            HttpResponseMessage message = null;

            HttpContent content = new StringContent(resource);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");
            myClient.Timeout = new TimeSpan(0, 0, 15);
            try
            {
                message = await myClient.PostAsync(url, content, token);
            }
            catch (WebException e)
            {
                message = CreateNotConnectedHttpResponse(e);
            }
            var responseContent = message.Content;
            var result = await responseContent.ReadAsStringAsync();
            Debug.WriteLine(result);
            return message;
        }

        private HttpResponseMessage CreateNotConnectedHttpResponse(WebException exception)
        {
            HttpResponseMessage httpDummy = new HttpResponseMessage();
            httpDummy.ReasonPhrase = NOT_CONNECTED_PHRASE;
            httpDummy.Content = new StringContent(exception.Message);
            httpDummy.StatusCode = HttpStatusCode.BadRequest;
            return httpDummy;

        }
    }
}

