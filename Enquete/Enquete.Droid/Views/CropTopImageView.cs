
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Util;

namespace Enquete.Droid
{
    public class CropTopImageView : ImageView
    {
        public CropTopImageView (Context context, IAttributeSet attrs)
            : base (context, attrs)
        {
            SetScaleType (ScaleType.Matrix);
        }

        protected override bool SetFrame (int l, int t, int r, int b)
        {

            Matrix matrix = this.ImageMatrix;
            float scaleFactor = this.Width / (float)this.Drawable.IntrinsicWidth;
            matrix.SetScale (scaleFactor, scaleFactor, 0, 0);
            this.ImageMatrix = matrix;
            return base.SetFrame (l, t, r, b);
        }
    }
}

