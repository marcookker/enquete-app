using System;
using Android.App;
using Android.Content;

namespace Enquete.Droid
{
    public delegate void OnTouch(Object o,EventArgs e);

    public class ProgressDialogWithTouchEvent : ProgressDialog
    {
        public ProgressDialogWithTouchEvent(Context context)
            : base(context)
        {  
            
        }

        public event OnTouch OnTouch;

        public override bool OnTouchEvent(Android.Views.MotionEvent e)
        {
            if (OnTouch != null)
            {
                OnTouch(this, new EventArgs());
            }
            return base.OnTouchEvent(e);
        }
    }
}

