using System;
using Android.Webkit;
using Android.Graphics;

namespace Enquete.Droid
{
    public delegate void FinishedLoadingWebPage (Object sender, EventArgs e);
    public delegate void UrlLoadingStarted (Object sender, EventArgs e, string url);
    public delegate void PageStarted (Object sender, EventArgs e, string url);

    public class WebViewClientWithLoadingEvents : WebViewClient
    {
        public WebViewClientWithLoadingEvents ()
        {
        }

        public event FinishedLoadingWebPage FinishedLoadingWebPage;
        public event UrlLoadingStarted UrlLoadingStarted;
        public event PageStarted PageStarted;
        private int running = 0;
        // Could be public if you want a timer to check.

        public override bool ShouldOverrideUrlLoading (WebView view, String urlNewString)
        {
            running++;
            view.LoadUrl (urlNewString);
            if (UrlLoadingStarted != null) {
                UrlLoadingStarted (this, new EventArgs (), urlNewString);
            }
            return true;
        }

        public override void OnPageStarted (WebView view, String url, Bitmap favicon)
        {
            if (UrlLoadingStarted != null) {
                UrlLoadingStarted (this, new EventArgs (), url);
            }
            running = Math.Max (running, 1); // First request move it to 1.
        }

        public override void OnPageFinished (WebView view, String url)
        {
            if (--running == 0) { // just "running--;" if you add a timer.
                // TODO: finished... if you want to fire a method.
                if (FinishedLoadingWebPage != null) {
                    FinishedLoadingWebPage (this, new EventArgs ());
                }
            }
        }
    }
}

