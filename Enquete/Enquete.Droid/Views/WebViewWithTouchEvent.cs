
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Util;

namespace Enquete.Droid
{
    public class WebViewWithTouchEvent : WebView
    {
        public WebViewWithTouchEvent (Context context, IAttributeSet attrs)
            : base (context, attrs)
        {

        }

        public event OnTouch OnTouch;

        public override bool OnTouchEvent (Android.Views.MotionEvent e)
        {
            if (OnTouch != null) {
                OnTouch (this, new EventArgs ());
            }
            return base.OnTouchEvent (e);
        }
    }


}

