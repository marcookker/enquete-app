
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Enquete.Droid
{
    [Application (Label = "@string/appname", AllowBackup = true/*, Icon = "@mipmap/icon"*/, Theme = "@android:style/Theme.NoTitleBar", Debuggable = true)]
    public class EnqueteApplication : Android.App.Application
    {
        public int Counter { get; set; } = 0;

        /// <summary>
        /// Base constructor which must be implemented if it is to successfully inherit from the Application
        /// class.
        /// </summary>
        /// <param name="handle"></param>
        /// <param name="transfer"></param>
        public EnqueteApplication (IntPtr handle, JniHandleOwnership transfer)
            : base (handle, transfer)
        {
            // do any initialisation you want here (for example initialising properties)
        }

        public override void OnCreate ()
        {
            base.OnCreate ();
            //app init ...
        }
    }
}
