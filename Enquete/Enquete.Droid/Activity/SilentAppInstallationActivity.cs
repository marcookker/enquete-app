
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using System.IO;

namespace Enquete.Droid
{
    [Activity]
    //    [IntentFilter(new []{ "android.app.action.ACTION_INSTALL_PACKAGE", Intent.ActionMain })]
    //    [IntentFilter(new []{ "android.app.action.BIND_DEVICE_ADMIN", Intent.ActionMain })]    	
    public class SilentAppInstallationActivity : Activity
    {
        Button installAppSilentlyButton;

        protected override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.SilentAppInstallationView);

            installAppSilentlyButton = FindViewById<Button> (Resource.Id.installAppSilentlyButton);
            installAppSilentlyButton.Click += delegate {
                //InstallAppSilently();
            };
            // Create your application here
        }

        //        private void InstallAppSilently()
        //        {
        //            //try
        //            //{
        //
        //            PackageManager pm = PackageManager;
        //            PackageInstaller pi = pm.PackageInstaller;
        //            PackageInstaller.SessionParams sp = new PackageInstaller.SessionParams(PackageInstallMode.FullInstall);
        //            Android.Content.PM.PackageInstaller.Session session;
        //            foreach (Android.Content.PM.PackageInstaller.SessionInfo nfo in pi.AllSessions)
        //            {
        //                try
        //                {
        //                    Android.Content.PM.PackageInstaller.Session ses = pi.OpenSession(nfo.SessionId);
        //
        //                }
        //                catch (Exception ex)
        //                {
        //                    System.Console.WriteLine(ex.StackTrace);
        //                }
        //            }
        //            sp.SetInstallLocation(PackageInstallLocation.InternalOnly);
        //            int sessId = pi.CreateSession(sp);
        //            string filepathApk = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/Download/Snake.apk";
        //            Console.WriteLine(filepathApk);
        //
        //            long sizeBytes = 0;
        //            FileInfo file = new FileInfo(filepathApk);
        //            sizeBytes = file.Length;
        //
        //            session = pi.OpenSession(sessId);
        //            using (Stream outpStream = session.OpenWrite("Snake", 0, sizeBytes))
        //            {
        //                using (FileStream inpStream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None))
        //                {
        //
        //                    //string[] sa = Directory.GetFiles(Android.OS.Environment.DataDirectory.Path) + "/com.epesi.enquete";
        //                    //            for (int i = 0; i < sa.Length; i++)
        //                    //            {
        //                    //                Console.WriteLine(sa[i]);
        //                    //            }
        //                    //
        //                    int total = 0;
        //
        //                    byte[] buffer = new byte[65536];
        //                    int c = 0;
        //                    while ((c = inpStream.Read(buffer, 0, c)) != -1)
        //                    {
        //                        total += c;
        //                        outpStream.Write(buffer, 0, c);
        //                    }
        //
        //                    session.Fsync(outpStream);
        //                    inpStream.Close();
        //                    outpStream.Close();
        //                    System.Console.WriteLine("InstallApkViaPackageInstaller - Success: streamed apk " + total + " bytes");
        //                }
        //            }
        ////
        ////
        ////            AwaitFile(file);
        //            Intent intent = new Intent(this, typeof(ReceivedCommitActivity));
        //            intent.AddFlags(ActivityFlags.NewTask);
        //            PendingIntent alarmtest = PendingIntent.GetActivity(this,
        //                                          0, intent, 0);
        //
        //
        //            
        //                      
        ////
        ////                // .. write updated APK file to out
        ////  
        ////      
        //                
        //                    
        //
        //                
        ////            }
        ////            catch (Exception ex)
        ////            {
        ////                System.Console.WriteLine(ex.StackTrace);
        ////            }
        //        }

        void AwaitFile (FileInfo file)
        {

            //While File is not accesable because of writing process
            while (IsFileLocked (file)) {
            }

            //File is available here

        }

        /// <summary>
        /// Code by ChrisW -> http://stackoverflow.com/questions/876473/is-there-a-way-to-check-if-a-file-is-in-use
        /// </summary>
        protected virtual bool IsFileLocked (FileInfo file)
        {
            FileStream stream = null;

            try {
                stream = file.Open (FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            } catch (IOException) {
                return true;
            } finally {
                if (stream != null)
                    stream.Close ();
            }

            //file is not locked
            return false;
        }


        // Broadcast receiver for receiving status updates from the IntentService
        private class StatusReceiver : BroadcastReceiver
        {
            // Prevents instantiation
            private StatusReceiver ()
                : base ()
            {

            }

            public override void OnReceive (Context context, Intent intent)
            {
                throw new NotImplementedException ();
            }
        }



    }
}

