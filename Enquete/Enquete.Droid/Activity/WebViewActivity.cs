using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using System.Threading;
using System.Threading.Tasks;
using Android.App.Admin;
using Android.Provider;
using Android.Content.PM;
using Android.Preferences;
using System.IO;
using Android.Media;

namespace Enquete.Droid
{
    [Activity (MainLauncher = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]

    //    kiosk
    //[IntentFilter (new [] { Intent.ActionMain },
    //    Categories = new [] { "android.intent.category.HOME", "android.intent.category.DEFAULT" })]
    public class WebViewActivity : Activity
    {
        private WebViewWithTouchEvent webView;
        private Diagnostics diagnostics;
        private ProgressDialogWithTouchEvent progress;
        private DevicePolicyManager mDpm;
        private long startTime;
        private long expired;

#if DEBUG
        private const int secondsIdle = 10;
#else
        private const int secondsIdle = 120;
#endif

        private WebViewClientWithLoadingEvents wClient;
        private string environmentDomain;
        private TextView mainBarTitleTextView;
        private bool isKVDT;
        private bool isADVISEUR;
        private string url;
        private bool screenRotated;
        private ImageButton soundButton;
        private bool soundOn;
        private AudioManager audioManager;
        //private Dictionary<string, int> currentSessionResults;

        protected override void OnSaveInstanceState (Bundle outState)
        {
            outState.PutBoolean ("screenRotated", screenRotated);
            base.OnSaveInstanceState (outState);
        }

        protected override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);
            if (savedInstanceState != null) {
                this.screenRotated = savedInstanceState.GetBoolean ("screenRotated");
            }
            this.Window.AddFlags (WindowManagerFlags.Fullscreen);

            // website and stats url, last part(device)
            Device device = Device.android;
            bool isTablet = Resources.GetBoolean (Resource.Boolean.isTablet);
            var devicePart = string.Empty;
            if (isTablet) {
                if (!screenRotated) {
                    RequestedOrientation = ScreenOrientation.Landscape;
                    this.screenRotated = true;
                    return;
                }
                device = Device.tab;
                devicePart = string.Empty;
            }
            //currentSessionResults = new Dictionary<string, int> ();

            // Control the volume of the device
            VolumeControlStream = Android.Media.Stream.Music;
            audioManager = (ApplicationContext
                .GetSystemService (Context.AudioService)).JavaCast<AudioManager> ();
            //domain of website and stats part

            isADVISEUR = GetADVISEURSettingIO ();
            environmentDomain = "content.ing.ujambo.nl";
            var envFromFile = GetEnvironmentDomainIO ();
            if (GetEnvironmentDomainIO () != "") {
                environmentDomain = envFromFile;
            }

            if (isTablet) {
                device = Device.samsung_tab;
                devicePart = string.Empty;
            } else {
                device = Device.samsung;
            }

            url = "http://" + environmentDomain + devicePart;

            //Services
            //EnqueteHttpService httpsvc = new EnqueteHttpService ();
            //ServiceContainer.Register<Diagnostics> (new Diagnostics (httpsvc, device));
            //diagnostics = ServiceContainer.Resolve<Diagnostics> ();

            //View layout stuff
            SetContentView (Resource.Layout.WebView);
            var footerIsMade = CreateFooterbarTextView ();
            CreateWebview (footerIsMade);
            CreateProgressView ();

            //Start the app loops
            ShowProgressAsync ();
            StartDelayedScreensaverAsync ();
            StartReloadingCacheAsync ();
        }



        private bool GetADVISEURSettingIO ()
        {
            var result = false;
            try {
                string path = Android.OS.Environment.GetExternalStoragePublicDirectory (Android.OS.Environment.DirectoryDcim).Path;
                string filename = Path.Combine (path, "ADVISEUR.txt");

                using (var streamReader = new StreamReader (filename)) {
                    string content = streamReader.ReadToEnd ();
                    System.Diagnostics.Debug.WriteLine (content);
                    result = Convert.ToBoolean (content);
                }
            } catch (Exception e) {

            }

            return result;
        }

        private string GetEnvironmentDomainIO ()
        {
            var result = "";
            try {
                string path = Android.OS.Environment.GetExternalStoragePublicDirectory (Android.OS.Environment.DirectoryDcim).Path;
                string filename = Path.Combine (path, "environmentDomain.txt");

                using (var streamReader = new StreamReader (filename)) {
                    string content = streamReader.ReadToEnd ();
                    System.Diagnostics.Debug.WriteLine (content);
                    result = content;
                }
            } catch (Exception e) {

            }

            return result;
        }




        private void CreateProgressView ()
        {
            progress = new ProgressDialogWithTouchEvent (this);

            progress.SetCancelable (false);
            progress.SetMessage ("De app is aan het laden...");
            progress.OnTouch += HandleOnProgressTouch;
        }

        private void CreateWebview (bool hasFooter)
        {
            webView = FindViewById<WebViewWithTouchEvent> (Resource.Id.webview);
            soundButton = FindViewById<ImageButton> (Resource.Id.soundButton);
            soundButton.SetImageDrawable (Resources.GetDrawable (Resource.Drawable.btn_volume_aan, null));
            soundOn = true;
            soundButton.Visibility = ViewStates.Invisible;
            soundButton.Click += delegate {
                if (soundOn) {
                    //turn sound off
                    audioManager.SetStreamVolume (Android.Media.Stream.System, 0, VolumeNotificationFlags.Vibrate);
                    audioManager.SetStreamVolume (Android.Media.Stream.Notification, 0, VolumeNotificationFlags.Vibrate);
                    audioManager.SetStreamVolume (Android.Media.Stream.Music, 0, VolumeNotificationFlags.Vibrate);
                    soundButton.SetImageDrawable (Resources.GetDrawable (Resource.Drawable.btn_volume_uit, null));
                    soundOn = false;
                } else {
                    //turn sound on
                    audioManager.SetStreamVolume (Android.Media.Stream.System, 10, VolumeNotificationFlags.PlaySound);
                    audioManager.SetStreamVolume (Android.Media.Stream.Notification, 10, VolumeNotificationFlags.PlaySound);
                    audioManager.SetStreamVolume (Android.Media.Stream.Music, 10, VolumeNotificationFlags.PlaySound);
                    soundButton.SetImageDrawable (Resources.GetDrawable (Resource.Drawable.btn_volume_aan, null));
                    soundOn = true;
                }
            };
            // (this, RelativeLayout.LayoutParams.MatchParent, 0, heightWeight);
            webView.LongClickable = false;
            webView.SetOnLongClickListener (new OnLongClickListener ());
            webView.Settings.JavaScriptEnabled = true;
            webView.OnTouch += HandleOnWebViewTouch;
            webView.Settings.DatabaseEnabled = true;
            webView.Settings.DomStorageEnabled = true;
            wClient = new WebViewClientWithLoadingEvents ();
            wClient.FinishedLoadingWebPage += HandleLoadFinishedAsync;
            wClient.UrlLoadingStarted += HandleUrlLoadingStarted;
            webView.SetWebViewClient (wClient);

            LoadUrl (url);
        }

        void HandleUrlLoadingStarted (object sender, EventArgs e, string url)
        {
            AddSoundButtonToWebview (url);
        }


        private void LoadUrl (string url)
        {
            Console.WriteLine ("url: " + url);
            webView.LoadUrl (url);
            AddSoundButtonToWebview (url);
        }

        private void AddSoundButtonToWebview (string url)
        {
            soundButton.Visibility = ViewStates.Invisible;
            if (url.Contains ("videowall") || url.Contains ("vimeo")) {
                Console.WriteLine ("Videowall");
                soundButton.Visibility = ViewStates.Visible;
            } else {
                Console.WriteLine ("no soundbutton");
            }
        }

        private bool CreateFooterbarTextView ()
        {
            var titleText = "Something went wrong";
            bool visible = false;
            mainBarTitleTextView = FindViewById<TextView> (Resource.Id.maintitlebartitle);
            if (environmentDomain == "content.ing.ujambo.nl") {
                titleText = "live";
            } else if (environmentDomain == "acc.content.ing.ujambo.nl") {
                titleText = "acc";
                visible = true;
            } else if (environmentDomain == "dev.content.ing.ujambo.nl") {
                titleText = "dev";
                visible = true;
            } else {
                mainBarTitleTextView.Text = "Something went wrong";
                visible = false;
            }
            mainBarTitleTextView.Text = "Env:" + titleText + " AppVersion:" + PackageManager.GetPackageInfo (PackageName, 0).VersionName;
            if (visible) {
                mainBarTitleTextView.Visibility = ViewStates.Visible;
            }
            return visible;
        }
        public override bool OnOptionsItemSelected (IMenuItem item)
        {
            switch (item.ItemId) {
            case Resource.Id.expanded_menu:
                StartActivity (typeof (PreferenceActivity));
                break;
            default:
                return base.OnOptionsItemSelected (item);
                break;
            }
            return true;
        }


        internal class OnLongClickListener : Java.Lang.Object, View.IOnLongClickListener
        {
            public bool OnLongClick (View v)
            {
                return true;
            }
        }
        public void CreateNonMovableStatusBar ()
        {
            if (!Settings.CanDrawOverlays (this.BaseContext)) {
                Intent intent = new Intent (Settings.ActionManageOverlayPermission,
                                    Android.Net.Uri.Parse ("package:" + PackageName));
                StartActivityForResult (intent, 1);
            } else {
                CreateStatusBar ();
            }
        }

        private void CreateStatusBar ()
        {
            if (Settings.CanDrawOverlays (this)) {
                IWindowManager manager = (ApplicationContext
                .GetSystemService (Context.WindowService)).JavaCast<IWindowManager> ();


                WindowManagerLayoutParams localLayoutParams = new WindowManagerLayoutParams ();
                localLayoutParams.Type = WindowManagerTypes.SystemError;
                localLayoutParams.Gravity = GravityFlags.Top;
                localLayoutParams.Flags = WindowManagerFlags.NotFocusable |

                // this is to enable the notification to recieve touch events
                WindowManagerFlags.NotTouchModal |

                // Draws over status bar
                WindowManagerFlags.LayoutInScreen;

                localLayoutParams.Width = WindowManagerLayoutParams.MatchParent;
                localLayoutParams.Height = (int)(50 * Resources.DisplayMetrics.ScaledDensity);
                localLayoutParams.Format = Android.Graphics.Format.Transparent;

                ConsumeTouch consume = new ConsumeTouch (this);

                manager.AddView (consume, localLayoutParams);
            }
        }

        //private void SendUserActivity ()
        //{
        //    CancellationTokenSource tokenSource = new CancellationTokenSource ();
        //    DateTime startDate = new DateTime (startTime).AddSeconds (secondsIdle);
        //    TimeSpan timeSpan = new TimeSpan (expired - startDate.Ticks);
        //    diagnostics.Send (tokenSource.Token, new StatRecord (new Details (Build.VERSION.Release, this.currentSessionResults.ToArray ()), timeSpan.TotalSeconds), this.environmentDomain, isKVDT, isADVISEUR);
        //    this.currentSessionResults.Clear ();
        //}

        private void EnableKioskMode (bool enabled)
        {
            try {
                if (enabled) {
                    if (mDpm.IsLockTaskPermitted (PackageName)) {

                        StartLockTask ();
                        Console.WriteLine ("Start Lock task");
                    }
                } else {
                    StopLockTask ();
                    Console.WriteLine ("Stop Lock task");
                }
            } catch (Exception e) {
                Console.WriteLine (e.Message);
                // TODO: Log and handle appropriately
            }
        }


        private async void HandleLoadFinishedAsync (Object sender, EventArgs e)
        {

            Console.WriteLine ("loadfinished");
            HideProgressAsync ();

        }

        private async void ShowProgressAsync ()
        {

            await Task.Delay (500);
            progress.Show ();

        }

        private async void StartReloadingCacheAsync ()
        {

            bool cacheIsVandaagAlGecleaned = false;
            while (true) {
                Console.WriteLine ("reload");
                await Task.Delay (10 * 60 * 1000);
                bool tussenkantoortijd = TussenKantoorTijd (DateTime.Now);
                if (!tussenkantoortijd) {
                    if (!cacheIsVandaagAlGecleaned) {
                        ShowProgressAsync ();

                        Console.WriteLine ("reload cache");

                        webView.ClearCache (true);
                        LoadUrl (url);
                        cacheIsVandaagAlGecleaned = true;
                    }
                } else {
                    cacheIsVandaagAlGecleaned = false;
                }
            }


        }

        private bool TussenKantoorTijd (DateTime datetime)
        {
            TimeSpan start = new TimeSpan (7, 0, 0);
            TimeSpan end = new TimeSpan (22, 0, 0);
            // convert datetime to a TimeSpan
            TimeSpan now = datetime.TimeOfDay;
            // see if start comes before end
            if (start < end)
                return start <= now && now <= end;
            // start is after end, so do the inverse comparison
            return !(end < now && now < start);
        }

        private async void HideProgressAsync ()
        {

            progress.Hide ();
        }

        private async void StartDelayedScreensaverAsync ()
        {

            Console.WriteLine ("startdelayed");
            startTime = DateTime.UtcNow.Ticks;
            this.expired = DateTime.UtcNow.AddSeconds (secondsIdle).Ticks;
            while (!IsTimerExpired ()) {
                await Task.Delay (500);
            }

            //SendUserActivity ();
            var myIntent = new Intent (this, typeof (ScreenSaverActivity));
            StartActivityForResult (myIntent, 0);

            //reloadwebpage
            ShowProgressAsync ();

            await Task.Delay (700);
            //webView.ClearCache(true);
            LoadUrl (url);

            Console.WriteLine ("start screensaver");
        }

        public void DelayScreensaver ()
        {
            DateTime currentTime = DateTime.UtcNow;
            this.expired = currentTime.AddSeconds (secondsIdle).Ticks;
        }

        public bool IsTimerExpired ()
        {
            bool isExpired = false;
            DateTime currentTime = DateTime.UtcNow;
            if (this.expired < currentTime.Ticks) {
                isExpired = true;
            }
            return isExpired;
        }
        //
        //        public override bool DispatchTouchEvent(MotionEvent e)
        //        {
        //            DelayScreensaver();
        //            return base.DispatchTouchEvent(e);
        //        }

        public override bool OnTouchEvent (MotionEvent e)
        {
            DelayScreensaver ();
            return base.OnTouchEvent (e);
        }

        public override void OnBackPressed ()
        {
            LoadUrl (url);
        }

        public void HandleOnWebViewTouch (Object o, EventArgs e)
        {
            //WebView.HitTestResult hr = webView.GetHitTestResult ();
            //if (hr.Extra != null) {
            //    int totalNumberOfClicks = 0;
            //    //if (currentSessionResults.TryGetValue (hr.Extra, out totalNumberOfClicks)) {
            //    //    totalNumberOfClicks++;
            //    //    currentSessionResults [hr.Extra] = totalNumberOfClicks;
            //    //} else {
            //    //    currentSessionResults.Add (hr.Extra, 1);
            //    //}
            //}
            DelayScreensaver ();
        }

        public void HandleOnProgressTouch (Object o, EventArgs e)
        {
            DelayScreensaver ();
        }

        protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult (requestCode, resultCode, data);
            if (requestCode == 0) {
                Console.WriteLine ("onresult");
                StartDelayedScreensaverAsync ();
            }
            if (requestCode == 1) {

                //CreateStatusBar();
            }
        }

        protected override void OnDestroy ()
        {
            Console.WriteLine ("destroy");
            base.OnDestroy ();
        }

        public override bool OnKeyDown (Keycode keyCode, KeyEvent e)
        {
            int musicVolume = audioManager.GetStreamVolume (Android.Media.Stream.Music);
            int maxVolume = audioManager.GetStreamMaxVolume (Android.Media.Stream.Music);
            switch (keyCode) {
            case Keycode.VolumeUp:
                if (musicVolume < maxVolume) {
                    soundButton.SetImageDrawable (Resources.GetDrawable (Resource.Drawable.btn_volume_aan, null));
                    audioManager.SetStreamVolume (Android.Media.Stream.Music, musicVolume + 1, VolumeNotificationFlags.PlaySound);
                    audioManager.SetStreamVolume (Android.Media.Stream.System, musicVolume + 1, VolumeNotificationFlags.PlaySound);
                    audioManager.SetStreamVolume (Android.Media.Stream.Notification, musicVolume + 1, VolumeNotificationFlags.PlaySound);
                    soundOn = true;
                }
                break;
            case Keycode.VolumeDown:
                if (musicVolume - 1 > 0) {
                    soundButton.SetImageDrawable (Resources.GetDrawable (Resource.Drawable.btn_volume_aan, null));
                    audioManager.SetStreamVolume (Android.Media.Stream.System, musicVolume - 1, VolumeNotificationFlags.PlaySound);
                    audioManager.SetStreamVolume (Android.Media.Stream.Notification, musicVolume - 1, VolumeNotificationFlags.PlaySound);
                    audioManager.SetStreamVolume (Android.Media.Stream.Music, musicVolume - 1, VolumeNotificationFlags.PlaySound);
                    soundOn = true;
                } else if (musicVolume - 1 == 0) {
                    soundButton.SetImageDrawable (Resources.GetDrawable (Resource.Drawable.btn_volume_uit, null));
                    audioManager.SetStreamVolume (Android.Media.Stream.Music, musicVolume - 1, VolumeNotificationFlags.Vibrate);
                    soundOn = false;
                }
                break;
            }
            return true;
        }

    }
}

