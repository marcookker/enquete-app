using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Enquete.Droid
{
    [Activity (Icon = "@mipmap/icon", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class ScreenSaverActivity : Activity
    {
        private enum ScreenSaverState
        {
            Default,
            Black
        }
        private Dictionary<ScreenSaverState, View> screensavers;

        private bool screenRotated;
        private bool running;
        private ScreenSaverState currentScreenSaverState;
        private View currentView;
        private bool isTablet;
        private Bitmap screensaverImage;
        protected override void OnSaveInstanceState (Bundle outState)
        {
            outState.PutBoolean ("screenRotated", screenRotated);
            base.OnSaveInstanceState (outState);
        }

        protected override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            running = true;
            isTablet = Resources.GetBoolean (Resource.Boolean.isTablet);

            if (isTablet) {
                RequestedOrientation = ScreenOrientation.Landscape;
                this.screenRotated = true;
            }
            VolumeControlStream = Android.Media.Stream.Music;

            Window.AddFlags (Android.Views.WindowManagerFlags.Fullscreen);
            //Window.AddFlags (Android.Views.WindowManagerFlags.LayoutNoLimits);
            Window.AddFlags (Android.Views.WindowManagerFlags.KeepScreenOn);

            screensavers = new Dictionary<ScreenSaverState, View> ();
            SetContentView (Resource.Layout.ScreenSaverView);
            ImageView defaultScreensaver = FindViewById<ImageView> (Resource.Id.enquetescreensaver);

            string path = Android.OS.Environment.GetExternalStoragePublicDirectory (Android.OS.Environment.DirectoryDcim).Path;
            string filename = System.IO.Path.Combine (path, "screensaver.png");
            screensaverImage = BitmapFactory.DecodeFile (filename);
            defaultScreensaver.SetImageBitmap (screensaverImage);

            currentScreenSaverState = ScreenSaverState.Default;

            currentView = defaultScreensaver;
            currentView.Visibility = ViewStates.Visible;



            screensavers.Add (ScreenSaverState.Default, defaultScreensaver);
            screensavers.Add (ScreenSaverState.Black, FindViewById<View> (Resource.Id.blackview));
            StartChangingScreensaver ();

        }


        public override bool OnTouchEvent (MotionEvent e)
        {
            Finish ();
            return base.OnTouchEvent (e);
        }

        protected override void OnResume ()
        {
            base.OnResume ();
        }

        public override void OnBackPressed ()
        {
            Finish ();
        }

        public override void Finish ()
        {
            running = false;
            base.Finish ();
        }

        public override bool OnKeyDown (Keycode keyCode, KeyEvent e)
        {
            return true;
        }

        private async void StartChangingScreensaver ()
        {
            while (running) {
                await Task.Delay (60 * 1000);
                EnqueteApplication mApp = ((EnqueteApplication)ApplicationContext);
                try {
                    SetScreenSaver (mApp);
                } catch {
                }
            }
        }

        private void SetScreenSaver (EnqueteApplication mApp)
        {
            bool tussenkantoortijd = TussenKantoorTijd (DateTime.Now);
            View newScreensaver = currentView;
            if (tussenkantoortijd) {
                //check if allready ing                
                if (screensavers.TryGetValue (ScreenSaverState.Default, out newScreensaver)) {

                    ((ImageView)newScreensaver).SetImageBitmap (screensaverImage);
                    currentScreenSaverState = ScreenSaverState.Default;
                }
            } else {
                if (screensavers.TryGetValue (ScreenSaverState.Black, out newScreensaver)) {
                    currentScreenSaverState = ScreenSaverState.Black;
                }
            }
            currentView = newScreensaver;
        }


        private bool Sunday (DateTime datetime)
        {
            if (datetime.DayOfWeek == DayOfWeek.Sunday) {
                return true;
            }
            return false;
        }

        private bool TussenKantoorTijd (DateTime datetime)
        {
            TimeSpan start = new TimeSpan (7, 0, 0);
            TimeSpan end = new TimeSpan (22, 0, 0);
            // convert datetime to a TimeSpan
            TimeSpan now = datetime.TimeOfDay;
            // see if start comes before end
            if (start < end)
                return start <= now && now <= end;
            // start is after end, so do the inverse comparison
            return !(end < now && now < start);
        }

        private bool ScreenBeveiligingsTijd (DateTime datetime)
        {
            TimeSpan start = new TimeSpan (3, 0, 0);
            TimeSpan end = new TimeSpan (4, 0, 0);
            // convert datetime to a TimeSpan
            TimeSpan now = datetime.TimeOfDay;
            // see if start comes before end
            if (start < end)
                return start <= now && now <= end;
            // start is after end, so do the inverse comparison
            return !(end < now && now < start);
        }
    }
}

