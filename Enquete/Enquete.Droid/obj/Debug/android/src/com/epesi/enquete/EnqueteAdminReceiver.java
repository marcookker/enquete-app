package com.epesi.enquete;


public class EnqueteAdminReceiver
	extends android.app.admin.DeviceAdminReceiver
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Enquete.Droid.EnqueteAdminReceiver, Enquete.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", EnqueteAdminReceiver.class, __md_methods);
	}


	public EnqueteAdminReceiver () throws java.lang.Throwable
	{
		super ();
		if (getClass () == EnqueteAdminReceiver.class)
			mono.android.TypeManager.Activate ("Enquete.Droid.EnqueteAdminReceiver, Enquete.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
