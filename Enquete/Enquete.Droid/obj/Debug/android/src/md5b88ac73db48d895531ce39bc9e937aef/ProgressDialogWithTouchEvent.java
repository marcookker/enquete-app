package md5b88ac73db48d895531ce39bc9e937aef;


public class ProgressDialogWithTouchEvent
	extends android.app.ProgressDialog
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onTouchEvent:(Landroid/view/MotionEvent;)Z:GetOnTouchEvent_Landroid_view_MotionEvent_Handler\n" +
			"";
		mono.android.Runtime.register ("Enquete.Droid.ProgressDialogWithTouchEvent, Enquete.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", ProgressDialogWithTouchEvent.class, __md_methods);
	}


	public ProgressDialogWithTouchEvent (android.content.Context p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == ProgressDialogWithTouchEvent.class)
			mono.android.TypeManager.Activate ("Enquete.Droid.ProgressDialogWithTouchEvent, Enquete.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}


	public boolean onTouchEvent (android.view.MotionEvent p0)
	{
		return n_onTouchEvent (p0);
	}

	private native boolean n_onTouchEvent (android.view.MotionEvent p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
